#include <windows.h>   // Для роботи з Windows API
#include <tchar.h>     // Для підтримки Unicode

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HANDLE hMutex;
HANDLE hMapFile;
LPVOID mappedData;

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720
#define USE_MUTEX 1

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
    const wchar_t CLASS_NAME[] = L"WINDOWS_CLASS";
    WNDCLASS wc = { 0 };
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;
    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(
        0,                      // Optional window styles.
        CLASS_NAME,             // Window class
        L"Виведення файлу даних у вікно", // Window text
        WS_OVERLAPPEDWINDOW,    // Window style
        CW_USEDEFAULT, CW_USEDEFAULT, SCREEN_WIDTH, SCREEN_HEIGHT,
        NULL,       // Parent window
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
    );

    if (hwnd == NULL)
    {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    MSG msg = { 0 };
    while (GetMessage(&msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_CREATE:
    {
        hMutex = CreateMutex(NULL, FALSE, L"Mutex_KI_21_1");
        if (hMutex == NULL)
        {
            MessageBox(NULL, L"Неможливо створити мутекс!", L"Помилка!", MB_ICONERROR | MB_OK);
            return -1;
        }

        hMapFile = OpenFileMapping(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, L"MemoryMapFileKI");
        if (hMapFile == NULL)
        {
            MessageBox(NULL, L"Помилка відкриття memory-mapped файлу!", L"Помилка!", MB_ICONERROR | MB_OK);
            return -1; // Повертаємо помилку
        }

        mappedData = (int*)MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 30 * sizeof(int));
        if (mappedData == NULL)
        {
            CloseHandle(hMapFile);
            MessageBox(NULL, L"Помилка відображення memory-mapped файлу в пам'ять!", L"Помилка!", MB_ICONERROR | MB_OK);
            return -1; // Повертаємо помилку
        }

        SetTimer(hwnd, 1, 500, NULL);
        break;
    }
    case WM_TIMER:
    {
        InvalidateRect(hwnd, NULL, TRUE);
        break;
    }
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hwnd, &ps);
        RECT clientRect;
        GetClientRect(hwnd, &clientRect);
        HBRUSH hBackgroundBrush = CreateSolidBrush(RGB(255, 255, 255));
        FillRect(hdc, &clientRect, hBackgroundBrush);
        DeleteObject(hBackgroundBrush);

        __try {
            WaitForSingleObject(hMutex, INFINITE);
            if (mappedData != NULL)
            {
                int* numbers = (int*)mappedData;
                int numCount = 30;
                wchar_t buff[128];
                for (int i = 0; i < numCount; i++)
                {
                    for (int j = 0; j < numbers[i]; j++)
                    {
                        buff[j] = '=';
                    }
                    buff[numbers[i]] = '\0';
                    TextOut(hdc, 10, 10 + i * 20, buff, wcslen(buff));
                }
            }
        }
        __finally {
            ReleaseMutex(hMutex);
        }

        EndPaint(hwnd, &ps);
        break;
    }
    case WM_CLOSE:
        if (MessageBox(hwnd, L"Really quit?", L"My application", MB_OKCANCEL) == IDOK)
        {
            DestroyWindow(hwnd);
            KillTimer(hwnd, 1);
            UnmapViewOfFile(mappedData);
            CloseHandle(hMutex);
            CloseHandle(hMapFile);
            PostQuitMessage(0);
        }
        return 0;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
