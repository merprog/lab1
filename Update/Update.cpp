﻿#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <time.h>

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));
    int iStart;
    HANDLE hMapFile = OpenFileMapping(FILE_MAP_READ | FILE_MAP_WRITE, FALSE, L"MemoryMapFileKI");
    if (hMapFile == NULL) {
        fprintf(stderr, "Помилка відкриття memory-mapped файлу!");
        return -1; // Повертаємо помилку
    }

    HANDLE hMutex = OpenMutex(SYNCHRONIZE, FALSE, L"MyMutex");
    if (hMutex == NULL)
    {
        fprintf(stderr, "Помилка відкриття м'ютекса!");
        CloseHandle(hMapFile);
        return -1; // Повертаємо помилку
    }

    // CloseHandle(hFile);
    int* mappedData = (int*)MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0,
        30 * sizeof(int));
    if (mappedData == NULL) {
        CloseHandle(hMapFile);
        fprintf(stderr, "Помилка відображення memory-mapped файлу в пам'ять!");
        CloseHandle(hMutex);
        return -1; // Повертаємо помилку
    }

    printf("Почати сортування?\n1 - yes, 0 - no\n> ");
    scanf_s("%d", &iStart);
    while (iStart)
    {
        WaitForSingleObject(hMutex, INFINITE);

        system("cls");
        for (int i = 0; i < 30; i++) {
            mappedData[i] = (int)((rand() % (100 - 10 + 1)) + 10);
            Sleep(1000);
        }
        printf("Масив створено. Створити новий?\n1 - yes, 0 - no\n> ");
        scanf_s("%d", &iStart);

        ReleaseMutex(hMutex);
    }

    system("cls");
    UnmapViewOfFile(mappedData);
    CloseHandle(hMapFile);
    CloseHandle(hMutex);
    return 0;
}
