﻿#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>
#include <windows.h>
#include <time.h>
#include "Common.h"

HANDLE hMutex;

void CreateFile()
{
    srand(time(0));
    int arr[30];
    for (unsigned i = 0; i < 30; i++) {
        arr[i] = (rand() % (100 - 10 + 1)) + 10;
    }

    // Отримуємо поточний каталог
    char currentDirectory[MAX_PATH];
    GetCurrentDirectoryA(MAX_PATH, currentDirectory);

    // Створюємо шлях до файлу "data.dat"
    strcat(currentDirectory, "\\data.dat");

    FILE* file = fopen(currentDirectory, "ab");
    if (file == NULL)
    {
        printf("\nError to create file!\n");
        return;
    }
    for (unsigned i = 0; i < 30; i++)
    {
        fwrite(&arr[i], sizeof(int), 1, file);
    }
    fclose(file);
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    hMutex = CreateMutex(NULL, FALSE, NULL);
    if (hMutex == NULL)
    {
        fprintf(stderr, "Error creating mutex\n");
        getchar(); // Затримка перед закриттям консолі
        return 1;
    }

    CreateFile();

    HANDLE hFile;
    hFile = CreateFile(L"data.dat", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Error opening file\n");
        getchar(); // Затримка перед закриттям консолі
        return 1;
    }
    HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READWRITE, 0, 0, L"MemoryMapFileKI");
    if (hMapFile == NULL) {
        fprintf(stderr, "Error creating memory-mapped file\n");
        CloseHandle(hFile);
        getchar(); // Затримка перед закриттям консолі
        return 1;
    }
    CloseHandle(hFile);
    int* mappedNumbers = (int*)MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0,
        0, 30 * sizeof(int));
    if (mappedNumbers == NULL) {
        fprintf(stderr, "Error mapping file to memory\n");
        CloseHandle(hMapFile);
        getchar(); // Затримка перед закриттям консолі
        return 1;
    }

    printf("Press 0 to exit\n");
    int a;
    scanf_s("%d", &a);
    fflush(stdin); // Очищення буфера вводу

    while (a != 0) {
        WaitForSingleObject(hMutex, INFINITE);
        printf(">>-------------start Sort numbers--------------\n");
        for (int i = 0; i < 30 - 1; i++)
        {
            for (int j = 0; j < 30 - i - 1; j++)
            {
                if (mappedNumbers[j] > mappedNumbers[j + 1])
                {
                    int temp = mappedNumbers[j];
                    mappedNumbers[j] = mappedNumbers[j + 1];
                    mappedNumbers[j + 1] = temp;
                }
            }
            Sleep(1500);
        }
        printf("Sorted numbers:\n");
        for (int j = 0; j < 30; j++)
        {
            printf("%d ", mappedNumbers[j]);
        }
        printf("\n");
        ReleaseMutex(hMutex);
        scanf_s("%d", &a);
        fflush(stdin); // Очищення буфера вводу
    }

    UnmapViewOfFile(mappedNumbers);
    CloseHandle(hMapFile);
    CloseHandle(hMutex);
    return 0;
}
